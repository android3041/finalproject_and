package com.sirapatson.final_project

import android.content.ContentValues
import android.content.ContentValues.TAG
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.sirapatson.final_project.databinding.FragmentEntermoneyBinding
import java.text.NumberFormat

class EntermoneyFragment : Fragment() {
    private var binding: FragmentEntermoneyBinding? = null
    private val sharedViewModel: CountViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentBinding = FragmentEntermoneyBinding.inflate(inflater, container, false)
        binding = fragmentBinding
        return fragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            enterMoneyFragment = this@EntermoneyFragment
        }
    }

    fun goTonextScreen(){
        val money = binding?.textInputEditText?.text.toString().toInt()
        sharedViewModel.setAmountMoney(money)
        Log.d(TAG, "Days = ${sharedViewModel.selectDay} , Money = ${money}")
        findNavController().navigate(R.id.action_entermoneyFragment_to_todaymoneyFragment)
    }

    fun goToOldScreen(){
        sharedViewModel.resetEverything()
        Log.d(TAG, "Reset finish")
        findNavController().navigate(R.id.action_entermoneyFragment_to_startFragment)
    }
}