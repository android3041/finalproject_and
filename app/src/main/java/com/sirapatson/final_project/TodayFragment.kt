package com.sirapatson.final_project

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.sirapatson.final_project.databinding.FragmentTodayBinding

class TodayFragment : Fragment() {
    private var binding: FragmentTodayBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentBinding = FragmentTodayBinding.inflate(inflater, container, false)
        binding = fragmentBinding
        return fragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            todayFragment = this@TodayFragment
        }
    }

    fun goToOldScreen(){
        findNavController().navigate(R.id.action_todaymoneyFragment_to_entermoneyFragment)
    }
}