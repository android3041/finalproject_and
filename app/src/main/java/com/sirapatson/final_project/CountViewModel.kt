 package com.sirapatson.final_project

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import java.text.NumberFormat

class CountViewModel : ViewModel() {
    private val _selectDay = MutableLiveData<Int>()
    val selectDay: LiveData<Int> = _selectDay

    private val _amountInput = MutableLiveData<Int>()
    val amountInput: LiveData<Int> = _amountInput

    private var moneyPerDay = 0

    //fun zone
    fun setNumDay(numberDay: Int) {
        _selectDay.value = numberDay
        Log.d("Select Day : ", _selectDay.value.toString())
    }

    fun setAmountMoney(money: Int) {
        _amountInput.value = money
        Log.d("Money User Input : ", _amountInput.value.toString())
    }

    fun resetEverything() {
        _selectDay.value = 0
        _amountInput.value = 0
    }

    init {
        resetEverything()
    }

    fun calAmountPerDay() {
        val total = (amountInput.value ?: 0) / (selectDay.value ?: 0)
        moneyPerDay = total
    }

}